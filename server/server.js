'use strict';

const TICK_LENGTH = 2;
const CLEANUP_INTERVAL = 1000;

let WebSocket = require('ws');
let WebRTC = require('wrtc');

let sockets = [];
const wss = new WebSocket.Server({ port: 8080 });
let last_id = 0;

console.log('second hollow server is online!');

const loopInterval = setInterval(() => {
  sockets.forEach((me) => {
    if (me.disconnect) return;
    sockets.forEach((them) => {
      if (them.disconnect) return;
      if (them.data.id !== me.data.id) {
        if (!them.data.dc || !me.data.dc) return;
        them.data.dc.send(JSON.stringify([me.data.id, me.data.position[0], me.data.position[1]]));
      }
    });
  });

}, TICK_LENGTH);

const cleanupInterval = setInterval(() => {
  sockets = sockets.filter((ws) => (ws.readyState === ws.OPEN || !ws.data.disconnect));
}, CLEANUP_INTERVAL);

wss.on('connection', (ws) => {
  ws.data = {id: last_id++, position: [0,0] };
  sockets.forEach((them) => {
    ws.send(JSON.stringify(them.voiceMessage));
  });
  ws.on('message', (message) => {
    message = JSON.parse(message);
    if (message.desc) { 
      receiveAnswer(ws, message.desc);
    }
    else if (message.candidate) {
      receiveCandidate(ws, message.candidate);
    }
    else if (message.voice) {
      ws.voiceMessage = message;
      relayMessage(ws, message);
    }

    else if (message.word) {
      relayMessage(ws, message);
    
    }
  });

  sockets.push(ws);
  sendOffer(ws);
});

function relayMessage(origin, message) {
  sockets.forEach((ws) => {
    message.id = origin.data.id;
    if (ws.readyState === ws.OPEN && ws !== origin) ws.send(JSON.stringify(message));
  });
}

function sendOffer(ws) {
  let configuration = { iceServers: [{ urls: 'stun:stun.l.google.com:19302' }] };
  let pc = new WebRTC.RTCPeerConnection(configuration);
  ws.data.pc = pc;

  pc.onicecandidate = function onicecandidate(candidate) {
    if (!candidate.candidate) return;
    console.log('candidate found: ', candidate.candidate);
    let json_candidate = JSON.stringify(candidate);
    ws.send(json_candidate);
  };

  pc.oniceconnectionstatechange = function oniceconnectionstatechange(event) {
    let state = pc.iceConnectionState;
    console.log(`ice state change: ${state}`);
    if (state === 'disconnected') {
      ws.data.disconnect = true;
    }
  };

  let dc = pc.createDataChannel('test');

  dc.onopen = function onopen() {
    console.log('data channel open');
    ws.data.dc = dc;
    dc.onmessage = function onmessage(event) {
      let position = JSON.parse(event.data);
      ws.data.position = position;
    };
  };

  pc.createOffer((desc) => {
    pc.setLocalDescription(desc);
    let json_desc = JSON.stringify({ desc: desc });
    ws.send(json_desc);
  }, (error) => { throw error; });

}

function receiveAnswer(ws, desc) {
  ws.data.pc.setRemoteDescription(desc);
}

function receiveCandidate(ws, candidate) {
  ws.data.pc.addIceCandidate(candidate);
}


