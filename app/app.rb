require 'roda'
require 'json'
require 'pry'

Rack::Mime::MIME_TYPES.merge!({ '.mjs' => 'application/javascript; charset=utf-8' })

class App < Roda
  plugin :render
  plugin :public
  plugin :json_parser
  plugin :sessions, secret: SECRETS[:roda_session_secret]

  route do |r|
    set_static_content(r)

    r.root do
      # view :welcome
      r.redirect '/game'
    end

    r.on 'game' do
      frontend[:websocket_url] = CONFIG[:websocket][:url]
      view :game
    end

    r.public

  end

  def frontend
    @frontend ||= {}
  end

  def set_static_content(r)
    @static_content = ""
    @static_content << "<link href='./pages#{r.path}/style.css' rel='stylesheet'>\n"
    @static_content << "<script src='./pages#{r.path}/main.mjs' type='module'></script>\n"
    @static_content
  end

  def static_content
    @static_content
  end

end
