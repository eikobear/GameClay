require 'yaml'
require 'pry'

app_path = "#{Dir.pwd}/config/application.yml" 
sec_path = "#{Dir.pwd}/config/secrets.yml"  

def load_yml(path)
  data = (File.exist?(path)) ? YAML.load_file(path) : { env: 'none' }
  env = data[:env].to_sym
  data.merge!(data[env]) if data[env]
  puts "loaded #{File.basename(path)} for environment: #{env}"
  data
end

CONFIG = load_yml(app_path)
SECRETS = load_yml(sec_path)

