
initially(() => {
  console.log('hello, gameclay.');
  window.backend = backend();
  let app = new PIXI.Application({ width: 256, height: 256 });
});

function backend() {
  let data = {}
  Array.from(document.getElementById('backend').children).forEach((child) => {
    data[child.className] = child.value;
  });
  return data
}

function initially(callback) {
  if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', callback);
  }
  else {
    callback();
  }
}

window.initially = initially;

