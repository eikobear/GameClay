
export default class Sprite {

  //TODO: parts and scene can be injected directly into variables instead of passed in
  make(parts, scene) {
    this.parts = parts;
    this.scene = scene;
    console.log('made a sprite!');
    this.speed = 1;
    this.velocity = { x: 0, y: 0 };
    createjs.Sound.registerSound('pages/game/snd/running.ogg', "running");
    this.defaultFrequency = (Math.random() * 400.0) + 150.0;
    this.voiceType = ['sine', 'square', 'sawtooth'][Math.floor(Math.random() * 3)];
    console.log(this.defaultFrequency);
    this.scene.handler.send({ voice: { type: this.voiceType, frequency: this.defaultFrequency } });
  }

  vocalize(word) {
    let lowerCount = word.replace(/[^a-z]/g, '').length;
    let upperCount = word.replace(/[^A-Z]/g, '').length;
    let count = lowerCount + upperCount;
    let screamFactor = (count > 0) ? upperCount / count : 0;
    let askMultiplier = 1 + word.replace(/[^\?]/g, '').length;
    let period = word.endsWith('.');
    let exclamation = word.endsWith('!');
    let sineAdjustor = (this.voiceType === 'sine') ? 0.1 : 0.0;
    if (exclamation) screamFactor *= 2;
    this.voice = new Voice(this.scene.handler.audioContext, {
      delay: 0,
      duration: 0.2 + (0.05 * count * (askMultiplier / 2.0)),
      fadein: 0.01,
      fadeout: 0.2,
      volume: 0.1 + (screamFactor * 0.2) + sineAdjustor,
      type: this.voiceType,
    });
    let f = (this.defaultFrequency + (screamFactor * 50.0) + ((Math.random() * 18.0) - 9));
    if (askMultiplier > 1) {
      this.voice.ask(f);
    }
    else if (period || exclamation) {
      this.voice.state(f);
    }
    else {
      this.voice.play(f);
    }

  }

  update(time) {
    window.sprite = this;
    let vel = this.normalize(this.velocity);
    this.parts.sprite.position.x += vel.x * this.speed * time;
    this.parts.sprite.position.y += vel.y * this.speed * time;

    let animation = this.parts.sprite.animation;
    if (vel.y > 0) animation = 'RunDown';
    if (vel.y < 0) animation = 'RunUp';
    if (vel.x != 0) animation = 'RunRight';
    if (vel.x < 0) {
      this.parts.sprite.sprite.scale.x = -1;
    }
    if (this.parts.sprite.animation !== animation) this.parts.sprite.animate(animation);
    if (vel.x == 0 && vel.y == 0) {
      if (!this.parts.sprite.paused()) {
        this.parts.sprite.pause();
        if (this.soundInstance) this.soundInstance.stop();
      }
    }
    else {
      if (vel.x >= 0) this.parts.sprite.sprite.scale.x = 1;
      if (this.parts.sprite.paused()) {
        this.parts.sprite.unpause();
        this.soundInstance = createjs.Sound.play('running', { loop: -1 });
      }
    }

  }

  normalize(vec) {
    let x = vec.x;
    let y = vec.y;
    let m = ((x * x) + (y * y)) ** 0.5;
    if (m === 0) return { x: 0, y: 0 };
    return { x: x / m, y: y / m };
  }

  move(vec) {
    if (vec.x != null) this.velocity.x = vec.x;
    if (vec.y != null) this.velocity.y = vec.y;
  }

  keypress(character, event) {
    if (!this.talking) {
      return;
    }

    if (character === "\r") {
      return;
    }

    if (character === ' ') {
      let word = this.input.send();
      this.vocalize(word.text.text);
    }
    else {
      this.input.text += character;
    }
  }

  keydown(key, event) {
    if (key === 'Enter') {
      if (this.talking) {
        this.talking = false;
        let word = this.input.close();
        this.vocalize(word.text.text);
        this.input = null;
      }
      else {
        this.talking = true;
        this.input = this.scene.make('Input');
        this.input.parts.sprite.position.x = this.parts.sprite.position.x - 16;
        this.input.parts.sprite.position.y = this.parts.sprite.position.y - 32;

      }
      return;
    }

    if (this.talking) {
      if (key === 'Backspace') {
        this.input.backspace();
      }
      return;
    }

    if (key === 'w') {
      this.move({ y: -1 });
    }

    if (key === 's') {
      this.move({ y: 1 });
    }

    if (key === 'a') {
      this.move({ x: -1 });
    }

    if (key === 'd') {
      this.move({ x: 1 });
    }
  }

  keyup(key, event) {
    if (key === 'w' || key === 's') {
      this.move({ y: 0 });
    }

    if (key === 'a' || key === 'd') {
      this.move({ x: 0 });
    }
  }

}

class Voice {
  constructor(context, options) {
    // TODO: create a defaults object and merge it with options
    this.context = context;
    this.delay = options.delay || 0;
    this.duration = options.duration || 1;
    this.volume = options.volume || 0.5;
    this.fadein = options.fadein || 0;
    this.fadeout = options.fadeout || 0;
    this.oscillator = this.context.createOscillator();
    this.gain = this.context.createGain();
    this.oscillator.type = options.type || 'sine';
    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime);
    this.oscillator.connect(this.gain);
    this.gain.connect(this.context.destination);
    this.oscillator.start(this.context.currentTime);
  }

  play(frequency) {

    this.gain.gain.cancelScheduledValues(this.context.currentTime);

    this.oscillator.frequency.setValueAtTime(frequency, this.context.currentTime);
    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime);

    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime + this.delay);

    this.gain.gain.exponentialRampToValueAtTime(this.volume, this.context.currentTime + this.delay + this.fadein);
    this.gain.gain.setValueAtTime(this.volume, this.context.currentTime + (this.duration) - this.fadeout);
    this.gain.gain.exponentialRampToValueAtTime(0.00001, this.context.currentTime + (this.duration));
  }

  ask(frequency) {

    this.gain.gain.cancelScheduledValues(this.context.currentTime);

    this.oscillator.frequency.setValueAtTime(frequency, this.context.currentTime);
    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime);

    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime + this.delay);

    this.gain.gain.exponentialRampToValueAtTime(this.volume, this.context.currentTime + this.delay + this.fadein);
    this.gain.gain.setValueAtTime(this.volume, this.context.currentTime + (this.duration) - this.fadeout);
    this.gain.gain.exponentialRampToValueAtTime(0.00001, this.context.currentTime + (this.duration));

    this.oscillator.frequency.exponentialRampToValueAtTime(frequency + 100.0, this.context.currentTime + (this.duration));
  }

  state(frequency) {

    this.gain.gain.cancelScheduledValues(this.context.currentTime);

    this.oscillator.frequency.setValueAtTime(frequency, this.context.currentTime);
    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime);

    this.gain.gain.setValueAtTime(0.00001, this.context.currentTime + this.delay);

    this.gain.gain.exponentialRampToValueAtTime(this.volume, this.context.currentTime + this.delay + this.fadein);
    this.gain.gain.setValueAtTime(this.volume, this.context.currentTime + (this.duration) - this.fadeout);
    this.gain.gain.exponentialRampToValueAtTime(0.00001, this.context.currentTime + (this.duration));

    this.oscillator.frequency.exponentialRampToValueAtTime(frequency - 100.0, this.context.currentTime + (this.duration));
  }


  loop(bpm) {
    const duration = 60.0 / bpm;

    this.loop = setInterval(() => {
      this.play(Voice.note2Frequency('a4'));
    }, duration * 1000);
  }

  riff(bpm, notestring) {
    const duration = 60.0 / bpm * 1000;
    const notes = notestring.split(' ');
    let unplayed = [...notes];

    this.riff = setInterval(() => {
      this.play(Voice.note2Frequency(unplayed.shift()));
      if(unplayed.length === 0) unplayed = [...notes];
    }, duration);
  }

  endLoop() {
    if(this.loop != null) clearInterval(this.loop);
    this.loop = null;
  }


  endRiff() {
    if(this.riff != null) clearInterval(this.riff);
    this.riff = null;
  }

  static note2Frequency(note) {
    const keys = 'cCdDefFgGaAb';
    const bkey = 'a';
    const boctave = 4;
    const bfrequency = 440.0;
    let [key, octave] = note;

    octave = Number(octave);

    let halfsteps = keys.indexOf(key) - keys.indexOf(bkey);
    halfsteps += (octave - boctave) * 12;

    let a = 2 ** (1 / 12);
    let frequency = bfrequency * (a ** halfsteps);
    return frequency;
  }
}
