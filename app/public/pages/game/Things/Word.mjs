
export default class Word {

  config() {
    return {
      sprite: false
    }
  }

  make(parts, scene) {
    this.parts = parts;
    this.scene = scene;
    this.lifespan = 5;
    this.age = 0;
    this.speed = 0.3;
    this.drift = (Math.random() / 5.0) - 0.1;
    this.velocity = { x: this.drift, y: -1 };
    this.text = new PIXI.Text('', { fontFamily: 'Comic Sans MS', fontSize: 14, fill: 0xaaaaaa, align: 'center' });
    this.parts.sprite.container.addChild(this.text);
    this.lastWord = null;
  }

  bump() {
    if (this._destroyed) return;
    if (!this.lastWord) return;
    if (this.lastWord._destroyed) { 
      this.lastWord = null;
      return;
    }


    let diff = this.parts.sprite.position.y - this.lastWord.parts.sprite.position.y;
    if (diff < 14) {
      this.lastWord.parts.sprite.position.y -= (14 - diff);
      this.lastWord.bump();
    }
  }

  update(time) {
    this.age += time;
    if (this.age > ((time * 60) * this.lifespan)) {
      this.scene.destroy(this);
      return;
    }

    let vel = this.normalize(this.velocity);
    this.parts.sprite.position.x += vel.x * this.speed * time;
    this.parts.sprite.position.y += vel.y * this.speed * time;
  }

  normalize(vec) {
    let x = vec.x;
    let y = vec.y;
    let m = ((x * x) + (y * y)) ** 0.5;
    if (m === 0) return { x: 0, y: 0 };
    return { x: x / m, y: y / m };
  }

}



