
export default class Input {

  config() {
    return {
      sprite: false
    }
  }

  make(parts, scene) {
    this.text = '';
    this.parts = parts;
    this.scene = scene;
    this.lifespan = 5;
    this.age = 0;
    this.speed = 0.1;
    this.velocity = { x: 0, y: -1 };
    this.textObject = new PIXI.Text('>', { fontFamily: 'Comic Sans MS', fontSize: 14, fill: 0xaaaaaa, align: 'center' });
    this.parts.sprite.container.addChild(this.textObject);
    this.lastWord = null;
  }

  backspace() {
    this.text = this.text.slice(0, -1);
  }

  send() {
    let word = this.scene.make('Word');
    word.text.text = this.text;
    word.parts.sprite.position.x = this.parts.sprite.position.x;
    word.parts.sprite.position.y = this.parts.sprite.position.y - 14;
    this.text = '';
    
    word.lastWord = this.lastWord;
    this.lastWord = word;
    word.bump();

    this.scene.handler.ws.send(JSON.stringify({ word: { 
      text: word.text.text, 
      x: word.parts.sprite.position.x, 
      y: word.parts.sprite.position.y, 
      drift: word.drift 
    } }));
    return word;
  }

  close() {
    let word = this.send();
    this.scene.destroy(this);
    return word;
  }

  update(time) {
    this.textObject.text = `>${this.text}`;
  }
}
