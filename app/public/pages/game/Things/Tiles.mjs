
export default class Tiles {

  make(parts, scene) {
    window.tiles = this;
    this.scene = scene;
    this.parts = parts;

    this.xbound = scene.clay.app.renderer.width / 2.0;
    this.ybound = scene.clay.app.renderer.height/ 2.0;
  }

  update(time) {

    if (this.scene.view.x - this.parts.sprite.position.x > this.xbound) {
      this.parts.sprite.position.x += 2 * this.xbound;
    }
    else if (this.scene.view.x - this.parts.sprite.position.x < -this.xbound) {
      this.parts.sprite.position.x -= 2 * this.xbound;
    }

    if (this.scene.view.y - this.parts.sprite.position.y > this.ybound) {
      this.parts.sprite.position.y += 2 * this.ybound;
    }
    else if (this.scene.view.y - this.parts.sprite.position.y < -this.ybound) {
      this.parts.sprite.position.y -= 2 * this.ybound;
    }

  }

}
