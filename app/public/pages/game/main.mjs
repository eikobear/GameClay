
import GameClay from './gameclay.mjs';

if (document.readyState === 'loading') {
  document.addEventListener('DOMContentLoaded', init);
}
else {
  init();
}

function init() {
  //import Scene from './scene.mjs';
  let clay = new GameClay('gameholder',
    { 
      width: 512,
      height: 512,
      backgroundColor: 0xaabbff
    });
  window.clay = clay;

  PIXI.loader.baseUrl = 'pages/game';

  clay.loadScene('Main', (scene) => {
    scene.start();
  });

  /*let sprite;
  let main = clay.loadPack('Main', (loader, resources) => {
    console.log('pack loaded');
    sprite = clay.packs['Main'].makeSprite('Sprite');
  
    clay.app.ticker.add((delta) => {
      let mouse = clay.app.renderer.plugins.interaction.mouse.global;
      sprite.sprite.x = mouse.x;
      sprite.sprite.y = mouse.y;
    });

  });
  */

}

