import Thing from './thing.mjs';
import View from './view.mjs';

export default class Scene {

  constructor(name, handler, pack) {
    this.name = name;
    this.handler = handler;
    this.pack = pack;
    this.clay = pack.clay;
  }

  load() {
    this.handler.load(this);
  }

  start() {
    this.view = new View(this);
    this.things = [];
    this.handler.start(this);
    this.ticker = new PIXI.Ticker();
    this.ticker.add((time) => {
      if (this.handler.update) this.handler.update(time);
      this.things.forEach((thing) => { 
        thing.update(time)
      });
    });

    window.addEventListener('keydown', this.handle_keydown.bind(this));
    window.addEventListener('keyup', this.handle_keyup.bind(this));
    window.addEventListener('keypress', this.handle_keypress.bind(this));

    this.ticker.start();
  }

  handle_keydown(event) {
    if (this.handler.keydown) this.handler.keydown(event.key, event);
    this.things.forEach((thing) => {
      thing.keydown(event.key, event);
    });
  }

  handle_keyup(event) {
    if (this.handler.keyup) this.handler.keyup(event.key, event);
    this.things.forEach((thing) => {
      thing.keyup(event.key, event);
    });
  }

  handle_keypress(event) {
    const character = String.fromCharCode(event.charCode);
    if (!event.charCode > 0 || !character) {
      return;
    }

    if (this.handler.keypress) this.handler.keypress(character, event);
    this.things.forEach((thing) => {
      thing.keypress(character, event);
    });
  }

  make(name) {
    let thing = new Thing(name, new this.pack.things[name](), this);
    this.things.push(thing);
    thing.handler.make({ sprite: thing.sprite }, this);
    return thing.handler;
  }

  destroy(handler) {
    handler.parts.sprite.container.parent.removeChild(handler.parts.sprite.container);
    handler.parts.sprite.container.destroy({ children: true });
    handler._destroyed = true;
    this.things = this.things.filter((thing) => (thing.handler !== handler));
  }

}
