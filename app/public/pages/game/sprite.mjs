
export default class Sprite {

  constructor(name, pack, scene, options = {}) {
    this.name = name;
    this.pack = pack;
    this.clay = pack.clay;
    this.container = new PIXI.Container();
    scene.view.container.addChild(this.container);
    if (options.hasSprite !== false) {
      this.animations = this.pack.getGroup(this.name);
      let defaultAnimation = Object.keys(this.animations)[0];
      this.sprite = new PIXI.extras.AnimatedSprite(this.animations[defaultAnimation]);
      this.animation = defaultAnimation;
      this.container.addChild(this.sprite);
      this.sprite.animationSpeed = 0.08;
      this.sprite.anchor.x = 0.5;
      this.sprite.anchor.y = 0.5;
      this.sprite.play();
    }
  }

  get position() {
    return this.container.position;
  }

  animate(animation, loop = true) {
    this.sprite.loop = loop;
    this.sprite.textures = this.animations[animation];
    this.animation = animation;
    this.sprite.play();
  }

  pause() {
    this.sprite.animationSpeed = 0;
  }

  unpause() {
    this.sprite.animationSpeed = 0.08;
  }

  paused() {
    return this.sprite.animationSpeed === 0;
  }

}
