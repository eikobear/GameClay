
export default class Main {

  load(scene) {
    console.log('loading main!');
    this.windCount = 5;
    for (let i = 0; i < this.windCount; i++) {
      createjs.Sound.registerSound(`pages/game/snd/wind/${i}.ogg`, `wind${i}`);
    }

    this.windDelay = (Math.random() * 5.0);
    this.nextWind = 0.0;
    this.wind = null;
    this.unsentMessages = [];
    //this.ws = new WebSocket('ws://localhost/server');
		this.ws = new WebSocket(backend.websocket_url)
    //this.ws = new WebSocket('ws://68.183.49.242/server');
    this.ws.onopen = (event) => {
      this.unsentMessages.forEach((message) => { this.ws.send(message); });
    }
    this.ws.onmessage = (event) => {
      let d = JSON.parse(event.data);
      if (d.desc) this.receiveOffer(d.desc);
      if (d.candidate) this.receiveCandidate(d.candidate);
      if (d.word) this.makeWord(d);
      if (d.voice) this.applyVoice(d);
    };
    window.scene = this;
    this.audioContext = new AudioContext();
  }

  send(message) {
    let json = JSON.stringify(message);
    if (this.ws.readyState === this.ws.CONNECTING) {
      this.unsentMessages.push(json);
    }
    else {
      this.ws.send(json);
    }
  }

  applyVoice(data) {
    let id = data.id;
    let other = this.others[id];
    if (other == null) {
      other = this.scene.make('Other');
      this.others[id] = other;
    }

    other.voiceType = data.voice.type;
    other.defaultFrequency = data.voice.frequency;
  }

  makeWord(data) {
    let id = data.id;
    let wordData = data.word;
    let word = this.scene.make('Word');
    word.text.text = wordData.text;
    word.parts.sprite.position.x = wordData.x;
    word.parts.sprite.position.y = wordData.y;
    word.velocity.x = wordData.drift;
    if (this.lastWords[id]) {
      word.lastWord = this.lastWords[id];
      word.bump();
    }
    this.lastWords[id] = word;
    this.others[id].vocalize(word.text.text);
  }

  receiveOffer(desc) {
    let configuration = { iceServers: [{ urls: 'stun:stun.l.google.com:19302' }] };
    this.pc = new RTCPeerConnection(configuration);

    this.pc.onicecandidate = (candidate) => {
      if (!candidate.candidate) return;
      let json_candidate = JSON.stringify(candidate);
      this.ws.send(json_candidate);
    };

    this.pc.oniceconnectionstatechange = (event) => {
      let state = this.pc.iceConnectionState;
      console.log(`ice state change: ${state}`);
    };

    this.pc.ondatachannel = (event) => {
      this.dc = event.channel;
      this.dc.onopen = () => {
        console.log('data channel open');
        this.dc.onmessage = (event) => {
          let [id, x, y] = JSON.parse(event.data);
          let other = this.others[id];
          if (other == null) {
            other = this.scene.make('Other');
            this.others[id] = other;
          }

          other.move(x, y);
        };
      };
    };

    this.pc.setRemoteDescription(desc);

    this.pc.createAnswer((local_desc) => {
      let json_desc = JSON.stringify({ desc: local_desc });
      this.ws.send(json_desc);
      this.pc.setLocalDescription(local_desc);
    }, (error) => { throw error; });

  }

  receiveCandidate(candidate) {
    this.pc.addIceCandidate(candidate);
  }

  start(scene) {
    this.scene = scene;
    console.log('starting main!');
    this.tiles = this.scene.make('Tiles');
    this.sprite = this.scene.make('Sprite');
    this.mask = this.scene.make('Mask');
    this.others = {};
    this.lastWords = {};
  }

  update(time) {
    let { x, y } = this.sprite.parts.sprite.position;
    this.scene.view.setPosition(x, y);
    this.mask.parts.sprite.position.x = x;
    this.mask.parts.sprite.position.y = y;

    if (this.wind == null) {
      this.nextWind += (time / 60.0);

      if (this.nextWind > this.windDelay) {
        this.nextWind = 0.0;
        this.windDelay = (Math.random() * 5.0);
        this.wind = createjs.Sound.play(`wind${ Math.floor(Math.random() * this.windCount) }`);
        this.wind.volume = Math.random() * 0.7 + 0.3;
        this.wind.on('complete', () => (this.wind = null));
      }
    }
    else {
      this.wind.volume *= 0.995;
    }

    if (this.dc) this.dc.send(JSON.stringify([x, y]));
  }

}
