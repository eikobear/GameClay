


export default class View {

  constructor(scene) {
    this.scene = scene;
    this.clay = scene.clay;
    this.container = new PIXI.Container();
    this.clay.app.stage.addChild(this.container);
    this.setPosition(0, 0);
  }

  setPosition(x, y) {
    this.x = x;
    this.y = y;

    this.container.x = -x + this.scene.clay.app.renderer.width / 2.0;
    this.container.y = -y + this.scene.clay.app.renderer.height / 2.0;
  }

}
