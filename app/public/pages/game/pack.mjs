
import Sprite from './sprite.mjs'

export default class Pack {

  constructor(name, clay, options) {
    options = options || {};
    this.clay = clay;
    this.name = name;
  }

  async load(callback) {
    this.things = {};
    this.path = `Packs/${this.name}/${this.name}.json`;
    this.loader = PIXI.loader.add(this.path).load(async () => {
      let things = this.listThings();
      for(let i = 0; i < things.length; i++) {
        let thing = things[i];
        let handler = await import(`./Things/${thing}.mjs`);
        this.things[thing] = handler.default;
        console.log('loaded', thing);
      }
      callback(this);
    });
    return this;
  }

  get(texture) {
    return this.loader.resources[this.path].textures[texture];
  }

  listThings() {
    let textures = this.loader.resources[this.path].textures;
    let things = [];
    for (let texturePath in textures) {
      let [groupName, animation, frame] = texturePath.split('/');
      if (!things.includes(groupName)) things.push(groupName);
    }

    //FIXME: this is NOT okay D:
    things.push('Other');
    things.push('Word');
    things.push('Input');
    return things;
  }

  getGroup(name) {
    let textures = this.loader.resources[this.path].textures;
    let group = {};
    for (let texturePath in textures) {
      let [groupName, animation, frame] = texturePath.split('/');
      if(groupName === name) {
        group[animation] = group[animation] || [];
        group[animation][Number(frame)] = textures[texturePath];
      }
    }

    return group;
  }

  makeSprite(name, scene, options = {}) {
    return new Sprite(name, this, scene, options);
  }

}
