


export default class Thing {

  constructor(name, handler, scene) {
    this.name = name;
    this.handler = handler;
    this.config = (handler.config) ? handler.config() : {};
    this.scene = scene;
    this.clay = scene.clay;
    //FIXME: passing in scene here is weird
    this.sprite = scene.pack.makeSprite(this.config.sprite || name, scene, { hasSprite: this.config.sprite !== false });
  }

  update(time) {
    if (this.handler.update) this.handler.update(time);
  }

  keydown(key, event) {
    if (this.handler.keydown) this.handler.keydown(key, event);
  }

  keyup(key, event) {
    if (this.handler.keyup) this.handler.keyup(key, event);
  }

  keypress(character, event) {
    if (this.handler.keypress) this.handler.keypress(character, event);
  }

}
