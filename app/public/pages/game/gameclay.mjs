console.log('in gameclay!');
import Pack from './pack.mjs';
import Scene from './scene.mjs';
console.log('defining main');
//import Main from './Scenes/Main.mjs';
console.log('defined!');
export default class GameClay {

  constructor(containerId, pixiOptions) {
    this.app = new PIXI.Application(pixiOptions);
    // pixel-perfect sprite positioning
    this.app.renderer.roundPixels = true;
    document.getElementById(containerId).append(this.app.view);

    this.packs = {};
    this.scenes = {};
    this.thingHandlers = {};
  }

  loadPack(name, callback) {
    let pack = this.packs[name] || new Pack(name, this);
    this.packs[name] = pack;
    pack.load(callback);
    return pack;
  }

  loadScene(name, callback) {
    import(`./Scenes/${name}.mjs`).then((handler) => {
      let pack = this.loadPack(name, () => {
        this.scenes[name] = new Scene(name, new handler.default(), pack);
        this.scenes[name].load();
        callback(this.scenes[name]);
      });
    });
  }

  loadThing(name, callback) {
    if (this.thingHandlers[name]) {
      callback(this.thingHandlers[name]);
      return;
    }

    import(`./Things/${name}.mjs`).then((handler) => {
      this.thingHandlers[name] = handler;
      callback(handler);
      return;
    });
  }

}
