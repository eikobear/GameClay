#!/bin/sh

HOST_DOMAIN='host.docker.internal'
ping -q -cl $HOST_DOMAIN > /dev/null 2>&1
if [ $? -ne 0 ]; then
	HOST_IP=$(ip route | awk 'NR==1 {print $3}')
	echo -e "$HOST_IP\t$HOST_DOMAIN" >> /etc/hosts
fi

exec /bin/parent caddy --conf /etc/Caddyfile --log stdout --agree=$ACME_AGREE
